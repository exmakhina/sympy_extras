#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2016,2024 Jérôme Carretero <cJ-sympy@zougloub.eu> & contributors
# SPDX-License-Identifier: BSD

import sympy

# Functions
x = sympy.Symbol("x", real=True)
degrees = sympy.Lambda(x, x * sympy.UnevaluatedExpr(180 / sympy.pi))
radians = sympy.Lambda(x, x * sympy.UnevaluatedExpr(sympy.pi / 180))
