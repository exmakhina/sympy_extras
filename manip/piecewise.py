#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2016,2024 Jérôme Carretero <cJ-sympy@zougloub.eu> & contributors
# SPDX-License-Identifier: BSD

import logging

import sympy


logger = logging.getLogger(__name__)


def apply(expr, expr_func=None, cond_func=None):
	if not isinstance(expr, sympy.Piecewise):
		return expr

	args = []
	for pwexpr, pwcond in expr.args:
		if expr_func:
			pwexpr_ = expr_func(pwexpr)
			pwexpr = pwexpr_
		if cond_func:
			pwcond_ = cond_func(pwcond)
			pwcond = pwcond_
			if pwcond == False:
				continue
		args.append((pwexpr, pwcond))

	return expr.func(*args)
