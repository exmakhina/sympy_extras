#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2016,2024 Jérôme Carretero <cJ-sympy@zougloub.eu> & contributors
# SPDX-License-Identifier: BSD

import logging

import sympy


logger = logging.getLogger(__name__)


def transformed(expr, func, exc_ok=False):
	if isinstance(expr, sympy.functions.elementary.piecewise.ExprCondPair):
		cexpr, ccond = expr
		res = expr.func(transformed(cexpr, func, exc_ok=exc_ok), transformed(ccond, func, exc_ok=exc_ok))

	elif isinstance(expr, sympy.core.basic.Atom):
		res = expr

	elif isinstance(expr, sympy.core.basic.Basic):
		args = []
		for arg in expr.args:
			arg = transformed(arg, func, exc_ok=exc_ok)
			args.append(arg)
		res = expr.func(*args)
		try:
			res = func(res)
		except:
			if not exc_ok:
				raise

	else:
		res = expr

	if res != expr:
		pass

	return res


def multipass(expr, *funcs, exc_ok=False):
	for func in funcs:
		logger.debug("Applying %s", func)
		expr = transformed(expr, func, exc_ok=exc_ok)
	return expr


def chained(*funcs):
	def f(arg):
		for func in funcs:
			arg = func(arg)
		return arg
	return f


def simplify_radicals_by_matching_bases(e):
	"""
	Find root power expressions that are clear roots of other expressions,
	that are found in the expression.

	For example, in our argument, we see sqrt(a²+2ab+b²) which has exponent 1/2,
	and we have also seen in our argument (a+b) which has the property
	 (a+b)**2 == a²+2ab+b²
	so we will replace sqrt(a²+2ab+b²) with (a+b).

	"""

	expressions = set()

	powers = set()
	def collect(x):
		if isinstance(x, sympy.Pow):
			exponent = x.args[1]
			if exponent.is_integer:
				pass
			elif exponent.is_negative:
				pass
			else:
				powers.add(x)
		else:
			expressions.add(x)
		return x

	transformed(e, collect)

	replacements = dict()

	for power in powers:
		base, exponent = power.args
		inv_exp = 1/exponent
		logger.debug("Finding expressions that elevated to power %s give %s", inv_exp, base)
		for expression in expressions:
			try:
				v = expression ** inv_exp
			except TypeError:
				continue
			if (v - base).simplify() == 0:
				replacements[power] = expression
	return e.subs(replacements)
