#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-sympy@zougloub.eu> & contributors
# SPDX-License-Identifier: BSD

import logging

import sympy


logger = logging.getLogger(__name__)


def normalize(e, func=lambda x: x):
	"""
	Simplify relationals inside an expression
	"""

	if not isinstance(e, sympy.core.relational.Relational):
		return e

	return e.func(func(e.lhs - e.rhs), 0)

def apply(expr, func=lambda x: x):
	if not isinstance(expr, sympy.core.relational.Relational):
		return expr

	return expr.func(func(_) for _ in expr.args)


def make_positives_sums(e):
	"""
	Given a relational expression, make collect terms so both hand sides are (more) positive
	"""

	if not isinstance(e, sympy.core.relational.Relational):
		return e

	lhs = []
	rhs = []

	s = e.lhs - e.rhs

	if isinstance(s, sympy.Add):

		for arg in s.args:
			if arg.is_positive:
				lhs.append(arg)
			else:
				rhs.append(-arg)


		return e.func(sympy.Add(*lhs), sympy.Add(*rhs))

	else:
		return e.func(s, 0)


def square_sides_if_positive(e):
	"""
	Given a relational expression with positive sides, square lhs and rhs
	"""

	if not isinstance(e, sympy.core.relational.Relational):
		return e

	if not e.lhs.is_positive and e.rhs.is_positive:
		return e

	return e.func(e.lhs**2, e.rhs**2)


def simplify_relational_by_positive_factor(e):
	"""
	Given a relational expression that contains a positive multiplier
	on both sides, simplify it by removing it.
	"""

	if not isinstance(e, sympy.core.relational.Relational):
		return e

	s = e.lhs - e.rhs

	if not isinstance(s, sympy.Mul):
		return e

	args = []
	for arg in s.args:
		if arg.is_positive:
			continue

		args.append(arg)

	return e.func(s.func(*args), 0)
