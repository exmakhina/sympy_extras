#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2016,2024 Jérôme Carretero <cJ-sympy@zougloub.eu> & contributors
# SPDX-License-Identifier: BSD


import sympy


def monkeypatch_add_relational_operators():
	"""
	Add dunder operators to Relational
	"""

	def add_x(self, other):
		return self.func(*[(_ + other) for _ in self.args])
	def sub_x(self, other):
		return self.func(*[(_ - other) for _ in self.args])
	def mul_x(self, other):
		all_positive = True
		for arg in self.args:
			if not arg.assumptions0["positive"]:
				all_positive = False
				break
		if all_positive:
			return self.func(*[(_ * other) for _ in self.args])
		else:
			raise ValueError("arguments are not all positive")
	sympy.core.relational.Relational.__add__ = add_x
	sympy.core.relational.Relational.__sub__ = sub_x
	sympy.core.relational.Relational.__mul__ = mul_x
	sympy.core.relational.Relational.__radd__ = add_x
	sympy.core.relational.Relational.__rsub__ = sub_x
	sympy.core.relational.Relational.__rmul__ = mul_x

